package br.com.senac.cachoeira.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.model.Cachoeira;

public class CachoeiraDAO extends SQLiteOpenHelper {


    private static final String DATABASE = "Cachoeira" ;
    private static final int VERSAO= 1 ;

    public CachoeiraDAO(Context context) {
        super(context, DATABASE, null, VERSAO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String ddl = "CREATE TABLE Cachoeira (" +
                "id Integer PRIMARY KEY ," +
                "nome TEXT NOT NULL ," +
                "informocoes TEXT NOT NULL ," +
                "imagem TEXT ," +
                "classificacao REAL ," +
                "email TEXT ," +
                "telefone TEXT ," +
                "endereco TEXT ," +
                "site TEXT  " +
                ");" ;
        db.execSQL(ddl);


    }


    public void salvar(Cachoeira cachoeira){

        ContentValues values = new ContentValues();
        values.put("nome" , cachoeira.getNome());
        values.put("informocoes" , cachoeira.getInformocoes());
        values.put("imagem" , cachoeira.getImagem());
        values.put("classificacao" , cachoeira.getClassificacao());
        values.put("email" , cachoeira.getEmail());
        values.put("telefone" , cachoeira.getTelefone());
        values.put("endereco" , cachoeira.getEndereco());
        values.put("site" , cachoeira.getSite());




        if(cachoeira.getId() == 0) {
            //Insert into a (c1,c2,c3 ) values ('d' , 'f' , 'fsdf') ;
            getWritableDatabase().insert("Cachoeira", null, values);
        }else{
            //update cahoeira set
            // c1 = ? ,
            // c2 = ? where id = ?
            getWritableDatabase().update("Cachoeira"
                    , values , "id =" + cachoeira.getId() , null);
        }
    }



    public List<Cachoeira> getLista(){
        List<Cachoeira> lista = new ArrayList<>() ;

        String[] colunas = { "id" , "nome" ,
                "informocoes" , "imagem" ,
                "classificacao" , "email" , "telefone" , "endereco" , "site" }  ;

       Cursor cursor =  getWritableDatabase().query("Cachoeira" , colunas , null , null, null ,null,null) ;

       while(cursor.moveToNext()){
           Cachoeira cachoeira = new Cachoeira() ;
           cachoeira.setId(cursor.getInt(0));
           cachoeira.setNome(cursor.getString(1));
           cachoeira.setInformocoes(cursor.getString(2));
           cachoeira.setImagem(cursor.getString(3));
           cachoeira.setClassificacao(cursor.getFloat(4));
           cachoeira.setEmail(cursor.getString(5));
           cachoeira.setTelefone(cursor.getString(6));
           cachoeira.setEndereco(cursor.getString(7));
           cachoeira.setSite(cursor.getString(8));

           lista.add(cachoeira);

       }




        return lista ;
    }





    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        String ddl = "DROP TABLE IF EXISTS Cachoeira ; " ;
        db.execSQL(ddl);
        this.onCreate(db);

    }
























}
